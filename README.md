Vaesper's PL9K theme.

This repo only initializes settings for Ben Hilburn's [powerlevel9k](https://github.com/bhilburn/powerlevel9k); make sure to source these settings **before** PL9K.
